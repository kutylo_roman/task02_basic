package com.kutylo;
/**
 * Class describes Fibonacci sequance.
 */

public class FibonacciNumbers {
    /**
     * main fibonacci array.
     */
    private int[] fibonacciArray;
    /**
     * number of odd elements.
     */
    private int oddNumbers;
    /**
     * number of even elements.
     */
    private int evenNumbers;
    /**
     * size of sequence.
     */
    private final int sizeSequence;
    /**
     * equals 100.
     */
    private final int maxPercent;

    /**
     *
     * @param sizeSequence Size of sequence
     * @param number1 First number of sequence
     * @param number2 Second number of sequence
     *
     */

    public FibonacciNumbers(final int sizeSequence, final int number1, final int number2){

        maxPercent = 100;

        this.sizeSequence = sizeSequence;

        fibonacciArray = new int[sizeSequence];
        fibonacciArray[0] = number1;
        fibonacciArray[1] = number2;

        for (int i = 2; i < sizeSequence; i++) {
            fibonacciArray[i] = fibonacciArray[i - 2] + fibonacciArray[i - 1];
        }

        oddNumbers = 0;
        evenNumbers = 0;

        calculatePercentages();
    }


    /**
     * Method calculates how many even and odd
     * numbers in sequence and set relevant variables.
     */
    private void calculatePercentages() {

        for (int i = 0; i < sizeSequence; i++) {
            if (fibonacciArray[i] % 2 == 0) {
                evenNumbers++;
            } else {
                oddNumbers++;
            }
        }
    }

    /**
     *
     * @return int[] array with fibonacci numbers.
     */
    public final int[] getFibonacciArray() {
        return fibonacciArray;
    }

    /**
     *
     * @return String about odd an even fibonacci numbers.
     */
    public final String getReport() {
        return ("\nOdd Fibonacci numbers sequence "
                + (double) oddNumbers / sizeSequence * maxPercent
                + '%' + ", even: " + (double) evenNumbers
                / sizeSequence
                * maxPercent + '%');
    }

}
