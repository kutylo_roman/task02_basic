package com.kutylo;

import java.util.Scanner;

/**
 * Class Range describes the range, the limits of which were entered by user.
 */
public class Range {
    /**
     * Start limit of range.
     */
    private int startRange;
    /**
     * End limit of range.
     */
    private int endRange;
    /**
     * max odd element of sequance.
     */
    private int maxOdd = 0;
    /**
     * max even element of sequance.
     */
    private int maxEven = 0;

    /**
     * Constructor of Range, which contains two limits: startRange and endRange.
     */
    Range() {
        startRange = 0;
        endRange = 0;
    }

    /**
     *
     * @param startRange The start limit of range
     * @param endRange The end limit of range
     */
    Range(final int startRange, final int endRange) {
        this.startRange = startRange;
        this.endRange = endRange;
        checkMaxValues();
    }


    /**
     * User enters range limits, then method checks max odd
     * and even values in range with method checkMaxValues.
     */
    public final void initRange() {
        System.out.println("Enter range limits: ");
        Scanner scanner = new Scanner(System.in);
        startRange = scanner.nextInt();
        endRange = scanner.nextInt();
        System.out.print("Entered range: " + '[' + startRange
                + ',' + endRange + "]\n");
        checkMaxValues();
    }

    /**
     * Method update values in maxEven and MaxOdd.
     */
    public final void checkMaxValues() {
        if (endRange % 2 == 0) {
            maxEven = endRange;
            maxOdd = endRange - 1;
        } else {
            maxOdd = endRange;
            maxEven = endRange - 1;
        }
    }

    /**
     * method prints odd numbers from start of fibonacci sequance.
     */
    public final void printOddNumbers() {
        System.out.println("Odd numbers from start: ");
        int sumNumbers = 0;
        for (int i = (startRange % 2 == 0) ? (startRange + 1) : startRange;
             i <= endRange; i += 2) {

            sumNumbers += i;
            System.out.print(i + " ");
        }
        System.out.println("\nSum of odd numbers: " + sumNumbers);
    }

    /**
     * method prints even numbers from end of fibonacci sequance.
     */
    public final void printEvenNumbers() {
        System.out.println("Even numbers from end: ");
        int sumNumbers = 0;
        for (int i = (endRange % 2 == 0) ? endRange : (endRange - 1);
             i >= startRange; i -= 2) {

            sumNumbers += i;
            System.out.print(i + " ");
        }
        System.out.println("\nSum of even numbers: " + sumNumbers);
    }

    /**
     *
     * @return max odd element from fibonacci sequance
     */
    public final int getMaxOdd() {
        return maxOdd;
    }

    /**
     *
     * @return max even element from fibonacci sequance
     */
    public final int getMaxEven() {
        return maxEven;
    }


}
