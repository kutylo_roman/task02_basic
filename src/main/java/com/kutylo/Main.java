package com.kutylo;

import java.util.Arrays;
import java.util.Scanner;

/**
 * The Main programm class.
 */

public class Main {
    /**
     * The lab program implements implements the application,
     * that form Fibonacci sequance.
     * Start numbers of Fibonacci sequance are last numbers
     * of entered by user range.
     *
     * @author Hurniak Yaroslav
     * @version 1.1.0
     * @param args You can enter some String parameters from command line
     */
    public static void main(final String[] args) {

        int fibonacciSize = 0;
        Range range = new Range();
        Scanner scanner = new Scanner(System.in);

        range.initRange();
        range.printOddNumbers();
        range.printEvenNumbers();


        System.out.println("Enter size of Fibonacci numbers "
                + "you want to build : ");
        fibonacciSize = scanner.nextInt();
        FibonacciNumbers fibonacciNumbers = new FibonacciNumbers(
                fibonacciSize, range.getMaxOdd(), range.getMaxEven());
        System.out.println("Builded Fibonacci numbers: ");
        System.out.println(Arrays.toString(
                fibonacciNumbers.getFibonacciArray()));
        System.out.println(fibonacciNumbers.getReport());

    }
}
